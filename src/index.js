import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Authentication from './auth'
import WalkThrough1 from './Walkthrough1'
import WalkThrough8 from './Walkthrough8'
import registerServiceWorker from './registerServiceWorker';
import WalkThrough2 from './Walkthrough2';
import WalkThrough3 from './Walkthrough3';
import WalkThrough4 from './Walkthrough4';
import WalkThrough5 from './Walkthrough5';
import WalkThrough6 from './Walkthrough6';
import WalkThrough7 from './Walkthrough7';
import AboutGame from './AboutGame';
import Profile from './Profile';
import PartnersResellers from './PartnersResellers';
import AboutUs from './AboutUs';
import ContactUs from './ContactUs';
import PrizeWinners from './PrizeWinners';
import Notifications from './Notifications';
import HistoryReciepts from './HistoryReceipts';
import Legal from './Legal';
import Auth from './auth';

ReactDOM.render(<Auth/>, document.getElementById('root'));
registerServiceWorker();
